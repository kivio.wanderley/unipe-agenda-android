package com.example.ekivuu.unipe_agenda;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ekivuu.unipe_agenda.model.Student;
import com.example.ekivuu.unipe_agenda.utils.StudentItem;

import java.util.List;
import java.util.Map;

/**
 * Created by EKivuu on 08/04/2017.
 */

public class MyListAdapter extends ArrayAdapter<StudentItem> {
    private final List<StudentItem> list;
    private LayoutInflater listInf;
    private Context c;

    public MyListAdapter(Context c, List<StudentItem> list) {
        super(c, R.layout.myentry, list);
        this.c = c;
        this.list = list;
        this.listInf = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public StudentItem getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.list.get(i).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LinearLayout listLay = (LinearLayout)listInf.inflate(R.layout.myentry, parent, false);

        TextView nameView = (TextView)listLay.findViewById(R.id.list_name);
        TextView idView = (TextView)listLay.findViewById(R.id.list_id);
        TextView phoneView = (TextView)listLay.findViewById(R.id.list_phone);
        ImageView imageView = (ImageView) listLay.findViewById(R.id.list_image);

        StudentItem currList = list.get(position);

        nameView.setText((String) currList.getNome());
        idView.setText(String.valueOf(currList.getId()));
        phoneView.setText(String.valueOf(currList.getPhone()));
        String photoImagePath = currList.getImagePath();

        if (null != photoImagePath && !photoImagePath.isEmpty()) {
            Bitmap bitmap = BitmapFactory.decodeFile(photoImagePath);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageBitmap(bitmap);
        }

        listLay.setTag(position);

        return listLay;
    }

}
