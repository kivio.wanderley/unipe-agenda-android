package com.example.ekivuu.unipe_agenda;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ekivuu.unipe_agenda.dao.StudentDAO;
import com.example.ekivuu.unipe_agenda.model.Student;
import com.example.ekivuu.unipe_agenda.utils.StudentsInsertUtil;

import java.io.File;

public class StudentsInsertActivity extends AppCompatActivity {


    private StudentsInsertUtil studentsInsertUtil;
    private Button captureImageButton;
    private String pathPhoto;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*Se teve um retorno OK da camera*/
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 567) {
                ImageView imageView = (ImageView) findViewById(R.id.imageViewId);
                Bitmap bitmap = BitmapFactory.decodeFile(pathPhoto);
//                Bitmap bitmapReduce = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(bitmap);
                imageView.setTag(pathPhoto);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_insert);

        captureImageButton = (Button) findViewById(R.id.camImageInsertId);

        studentsInsertUtil = new StudentsInsertUtil(StudentsInsertActivity.this);

        Intent intent = getIntent();
        Student editStudent = (Student) intent.getSerializableExtra("student");

        if (editStudent != null){
            studentsInsertUtil.buildEditStudent(editStudent);
        }

        captureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCaptureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                pathPhoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File filePhoto = new File(pathPhoto);
                intentCaptureImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(filePhoto));
                startActivityForResult(intentCaptureImage, 567);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.this_menu_insert, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.StudentsInsert_MenuOk:
                Student student = new Student();
                try {
                    StudentDAO studentDAO = new StudentDAO(StudentsInsertActivity.this);
                    student = studentsInsertUtil.buildStudentForInsert(pathPhoto);

                    if (student.getId() != null){
                        studentDAO.update(student);
                    }else{
                        studentDAO.create(student);
                    }

                    studentDAO.close();
                    Toast.makeText(StudentsInsertActivity.this, "Novo aluno " + student.getName() + " salvo!", Toast.LENGTH_SHORT).show();
                    finish();
                }catch (Exception e){
                    Toast.makeText(StudentsInsertActivity.this, "Erro ao salvar novo aluno. \n" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
