package com.example.ekivuu.unipe_agenda;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.example.ekivuu.unipe_agenda.dao.StudentDAO;
import com.example.ekivuu.unipe_agenda.model.Student;
import com.example.ekivuu.unipe_agenda.utils.StudentItem;

public class StudentsListActivity extends AppCompatActivity {

    ListView studentListView;
    private List<StudentItem> studentsList;
    private StudentDAO studentDAO;

    @Override
    protected void onResume() {
        super.onResume();
        buildStudentsList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);
        studentDAO = new StudentDAO(StudentsListActivity.this);

        studentListView = (ListView) findViewById(R.id.studentsList_listViewStudents);

        studentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {
                StudentItem entry = (StudentItem) studentListView.getItemAtPosition(position);
                Student student = studentDAO.getStudentById((Long) entry.getId());

                Intent intentStudentInsert = new Intent(StudentsListActivity.this, StudentsInsertActivity.class);
                intentStudentInsert.putExtra("student", student);
                startActivity(intentStudentInsert);
            }
        });

        Button newButton = (Button) findViewById(R.id.studentsInsert_buttonNew);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentStudentInsert = new Intent(StudentsListActivity.this, StudentsInsertActivity.class);
                startActivity(intentStudentInsert);
            }
        });

        registerForContextMenu(studentListView);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 123) {
            System.out.println("CHAMADA");
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        final MenuItem callMenuItem = menu.add("Ligar");
        MenuItem smsMenuItem = menu.add("Enviar SMS");
        final MenuItem goToSiteMenuItem = menu.add("Visitar Site");
        MenuItem locationMenuItem = menu.add("Abrir Local");
        MenuItem deleteMenuItem = menu.add("Delete");

        AdapterView.AdapterContextMenuInfo adapterMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        StudentItem entry = (StudentItem) studentListView.getItemAtPosition(adapterMenuInfo.position);
        final Student student = studentDAO.getStudentById((Long) entry.getId());

        Intent goToSiteIntent = new Intent(Intent.ACTION_VIEW);
        goToSiteIntent.setData(Uri.parse("http://"+student.getSite()));
        goToSiteMenuItem.setIntent(goToSiteIntent);

        Intent smsSendIntent = new Intent(Intent.ACTION_VIEW);
        smsSendIntent.setData(Uri.parse("sms:"+student.getNumber()));
        smsMenuItem.setIntent(smsSendIntent);

        Intent locationIntent = new Intent(Intent.ACTION_VIEW);
        locationIntent.setData(Uri.parse("geo:0,0?q="+student.getAddress()));
        locationMenuItem.setIntent(locationIntent);



        callMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (ActivityCompat.checkSelfPermission(StudentsListActivity.this, android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(StudentsListActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE}, 123);
                }
                else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+student.getNumber()));
                    callMenuItem.setIntent(callIntent);
                }
                return false;
            }
        });

//        smsMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                return false;
//            }
//        });
//
//        goToSiteMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                return false;
//            }
//        });

        deleteMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                studentDAO.delete(student.getId());

                buildStudentsList();

                Toast.makeText(StudentsListActivity.this, "Aluno "  + student.getName() + " removido!", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    private void buildStudentsList() {

        StudentDAO studentDAO = new StudentDAO(StudentsListActivity.this);
        this.studentsList = studentDAO.readNames();
        List<StudentItem> studentList = new ArrayList<StudentItem>(studentsList);
        studentDAO.close();

        MyListAdapter studentsListViewAdapter =
                new MyListAdapter(this, studentList);
        studentListView.setAdapter(studentsListViewAdapter);

    }


}
