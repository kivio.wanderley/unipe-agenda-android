package com.example.ekivuu.unipe_agenda.utils;

import java.util.Map;

/**
 * Created by EKivuu on 08/04/2017.
 */

public class StudentItem {
    private long id;
    private String nome;
    private String imagePath;
    private String phone;

    public StudentItem(long id, String nome, String imagePath, String phone) {
        this.id = id;
        this.nome = nome;
        this.imagePath = imagePath;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
